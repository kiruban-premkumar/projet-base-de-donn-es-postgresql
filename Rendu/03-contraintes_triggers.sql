-- TRIGGERS -- 

CREATE FUNCTION update_stock() RETURNS trigger AS $update_stock$
	BEGIN
		IF (TG_OP = 'INSERT') THEN
			UPDATE Stock SET qte_actuelle=qte_actuelle-NEW.quantite WHERE ref_produit=NEW.ref_produit;
		ELSIF (TG_OP = 'UPDATE') THEN
			UPDATE Stock SET qte_actuelle=qte_actuelle+OLD.quantite-NEW.quantite WHERE ref_produit=NEW.ref_produit;
		END IF;
		RETURN NEW;
	END;
$update_stock$ LANGUAGE plpgsql;

CREATE TRIGGER update_stock BEFORE INSERT OR UPDATE ON  Ligne_commande
	FOR EACH ROW EXECUTE PROCEDURE update_stock();