
INSERT INTO Categorie(ref_categorie,libelle,description,CA) VALUES
('CAT0001','INFORMATIQUE','Section informatique, pc portables, ordinateur de bureau, netbook',0),
('CAT0002', 'TV/SON/HIFI','Catégorie Image et son tv et lecteurs dvd',0),
('CAT0003','AUTO/MOTO/GPS','Equipement automobile et gps : audio (autoradio, enceinte) , vidéo (baladeur multimédia, console de jeux), gps, téléphonie ( bluetooth voiture)',0);

INSERT INTO Sous_categorie(ref_sous_categorie,libelle,description,CA,ref_categorie) VALUES
('SCAT0001','PC Portables', 'Asus, Apple, HP, THOSIBA',0,'CAT0001'),
('SCAT0002','Disques durs', 'Seagate, western digital',0,'CAT0001'),
('SCAT0003','Télévisions', 'Catégorie TV LCD',0,'CAT0002'),
('SCAT0004','Lecteurs DVD', 'Profitez de notre sélection de lecteur dvd',0,'CAT0002'),
('SCAT0005','GPS', 'catégorie GPS equipement automobile & gps',0,'CAT0003'),
('SCAT0006','Autoradio', 'catégorie Autoradio equipement automobile',0,'CAT0003');

INSERT INTO Categorie_speciale(ref_categorie_speciale,libelle,description,CA,type) VALUES
('CATS001','DISCOUNT','les offres discounts',0,'derniers achetés'),
('CATS002','TOP VENTE','les offres les plus vendus',0,'plus vendus');

INSERT INTO Adresse(ref_adresse,batiment,numero,voie,adresse_1,adresse_2,code_postal,ville,pays) VALUES
('ADR0001','Bat. 5',1,'Rue','Charles de Gaulle', 'ZAC MARINIERE','91070','BONDOUFLE','FRANCE'),
('ADR0002','2nd Building',2,'Way','Furzeground','Stockley Park','W1S 1YZ','LONDON','United Kingdom');

INSERT INTO Fournisseur(ref_fournisseur,description,ref_adresse) VALUES
('APPLE','une entreprise multinationale américaine qui conçoit et commercialise des produits électroniques grand public, des ordinateurs personnels','ADR0002'),
('SUPRATEC','Supratec est une entreprise qui a pour domaine d''activité principal d''apporter des solutions de productivité aux entreprises','ADR0001');


INSERT INTO Produit(ref_produit,libelle,description,d_premier_achat,CA,categorie,ref_fournisseur) VALUES
('PRODUCTMBA0001','MAC BOOK AIR','Intel Core i5 Dual-Core (1,7 GHz) - SSD 128 Go - RAM 4096 Mo - Intel HD Graphics 4000 - OS X Lion','2012-03-01',0,'CAT0001','APPLE');

INSERT INTO Categorie_speciale_produit(ref_categorie,ref_produit) VALUES
('CAT0001','PRODUCTMBA0001');

INSERT INTO Evenement(ref_evenement,libelle,description,CA) VALUES
('NOEL001','NOEL','offres speciales noel',0);

INSERT INTO Promo(ref_evenement,ref_produit,taux,d_debut,d_fin) VALUES
('NOEL001','PRODUCTMBA0001',15,'2013-12-01','2013-12-31');

INSERT INTO Stock(ref_produit,qte_min,qte_max,qte_limite,qte_actuelle) VALUES
('PRODUCTMBA0001',500,10000,5000,2000);

INSERT INTO Client(ref_client,nom,prenom,d_naissance) VALUES
('CL000001','PREMKUMAR','Kiruban','1989-08-01'),
('CL000002','NAGALINGAM','Ravi','1992-02-26'),
('CL000003','SOUISSI','Amin','1990-01-01');

INSERT INTO Cadeau(ref_cadeau,prix_min,ref_produit) VALUES
('CADO0001',100000,'PRODUCTMBA0001');

INSERT INTO Commande(ref_commande,etat,montant) VALUES
('CMD00001','V',1300);

INSERT INTO Ligne_commande(ref_commande,ref_ligne,quantite,prix,ref_produit) VALUES
('CMD00001','LI0001',1,1300,'PRODUCTMBA0001');

INSERT INTO Panier_recurrent(ref_commande,ref_client) VALUES
('CMD00001','CL000001');

INSERT INTO C_client(ref_commande,d_commande,d_livraison,ref_client,ref_cadeau) VALUES
('CMD00001','2013-03-10','2013-03-23','CL000001',NULL);

INSERT INTO C_fournisseur(ref_commande,d_commande,d_livraison,ref_fournisseur) VALUES
('CMD00001','2013-02-01','2013-02-12','APPLE');