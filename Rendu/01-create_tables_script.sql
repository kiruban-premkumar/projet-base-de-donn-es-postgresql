
/* Pour faciliter la création des tables/réinitialisation de la base de données */

DROP TYPE IF EXISTS etat_enum CASCADE;
DROP TYPE IF EXISTS speciale_enum CASCADE;
DROP TYPE IF EXISTS voie_enum CASCADE;

DROP TABLE IF EXISTS Categorie, Sous_categorie, Categorie_speciale, Categorie_speciale_produit CASCADE;
DROP TABLE IF EXISTS Evenement, Promo, Produit, Stock CASCADE;
DROP TABLE IF EXISTS Client, Fournisseur, Adresse, Cadeau, Commande CASCADE;
DROP TABLE IF EXISTS Ligne_commande, Panier_recurrent, C_client, C_fournisseur CASCADE;

/* Créations des tables */

CREATE TABLE Categorie(
	ref_categorie varchar(50) PRIMARY KEY,
	libelle varchar(30) NOT NULL,
	description text,
	CA integer
);

CREATE TABLE Sous_categorie(
	ref_sous_categorie varchar(50) PRIMARY KEY,
	libelle varchar(30) NOT NULL, 
	description text, 
	CA integer,
	ref_categorie varchar(50) NOT NULL
);

CREATE TYPE speciale_enum AS ENUM ('derniers achetés', 'plus vendus');
CREATE TABLE Categorie_speciale(
	ref_categorie_speciale varchar(50) PRIMARY KEY,
	libelle varchar(30) NOT NULL, 
	description text, 
	CA integer,
	type speciale_enum
);

CREATE TABLE Categorie_speciale_produit(
	ref_categorie varchar(50) NOT NULL,
	ref_produit varchar(50) NOT NULL
);

CREATE TABLE Evenement(
	ref_evenement varchar(50) PRIMARY KEY,
	libelle varchar(30) NOT NULL, 
	description text, 
	CA integer
);

CREATE TABLE Promo(
	ref_evenement varchar(50) NOT NULL,
	ref_produit varchar(50) NOT NULL,
	taux integer,
	d_debut date,
	d_fin date
);

CREATE TABLE Produit(
	ref_produit varchar(50) PRIMARY KEY,
	libelle varchar(30) NOT NULL,
	description text,
	d_premier_achat date,
	CA integer,
	categorie varchar(50) NOT NULL,
	ref_fournisseur varchar(50) NOT NULL
);

CREATE TABLE Stock(
	ref_produit varchar(50) NOT NULL,
	qte_min integer,
	qte_max integer,
	qte_limite integer,
	qte_actuelle integer
);

CREATE TABLE Client(
	ref_client varchar(50) PRIMARY KEY,
	nom varchar(30) NOT NULL,
	prenom varchar(30) NOT NULL,
	d_naissance date
);

CREATE TABLE Fournisseur(
	ref_fournisseur varchar(50) PRIMARY KEY,
	description text,
	ref_adresse varchar(50) NOT NULL
);

CREATE TYPE voie_enum AS ENUM ('Way', 'Rue', 'Boulevard', 'Avenue', 'Allée', 'Chemin', 'Impasse', 'Lieu-dit', 'Square');
CREATE TABLE Adresse(
	ref_adresse varchar(50) PRIMARY KEY,
	batiment varchar(30),
	numero integer,
	voie voie_enum,
	adresse_1 varchar(30),
	adresse_2 varchar(30),
	code_postal varchar(30),
	ville varchar(30),
	pays varchar(30)
);

CREATE TABLE Cadeau(
	ref_cadeau varchar(50) PRIMARY KEY,
	prix_min integer,
	ref_produit varchar(50) NOT NULL
);

CREATE TYPE etat_enum AS ENUM ('ECC','V','ECP','ECL','L');
CREATE TABLE Commande(
	ref_commande varchar(50) PRIMARY KEY,
	etat etat_enum,
	montant integer
);

CREATE TABLE Ligne_commande(
	ref_commande varchar(50) NOT NULL,
	ref_ligne varchar(50) PRIMARY KEY,
	quantite integer,
	prix integer,
	ref_produit varchar(50) NOT NULL
);

CREATE TABLE Panier_recurrent(
	ref_commande varchar(50) NOT NULL,
	ref_client varchar(50) NOT NULL
);

CREATE TABLE C_client(
	ref_commande varchar(50) NOT NULL,
	d_commande date,
	d_livraison date,
	ref_client varchar(50) NOT NULL,
	ref_cadeau varchar(50)
);

CREATE TABLE C_fournisseur(
	ref_commande varchar(50) NOT NULL,
	d_commande date,
	d_livraison date,
	ref_fournisseur varchar(50) NOT NULL
);

/* Ajouts des contraintes */

ALTER TABLE Categorie 
ADD CONSTRAINT ck_Categorie 
UNIQUE(ref_categorie);

ALTER TABLE Sous_categorie
ADD CONSTRAINT ck_Sous_Categorie UNIQUE(ref_sous_categorie),
ADD CONSTRAINT fk_Sous_Categorie_Categorie FOREIGN KEY (ref_categorie) REFERENCES Categorie(ref_categorie);

ALTER TABLE Categorie_speciale
ADD CONSTRAINT ck_Categorie_Speciale UNIQUE(ref_categorie_speciale);

ALTER TABLE Categorie_speciale_produit
ADD CONSTRAINT fk_Categorie_Speciale_Produit_Categorie FOREIGN KEY (ref_categorie) REFERENCES Categorie(ref_categorie),
ADD CONSTRAINT fk_Categorie_Speciale_Produit_Produit FOREIGN KEY (ref_produit) REFERENCES Produit(ref_produit);

ALTER TABLE Evenement
ADD CONSTRAINT ck_Evenement UNIQUE(ref_evenement);

ALTER TABLE Promo
ADD CONSTRAINT fk_Promo_Evenement FOREIGN KEY (ref_evenement) REFERENCES Evenement(ref_evenement),
ADD CONSTRAINT fk_Promo_Produit FOREIGN KEY (ref_produit) REFERENCES Produit(ref_produit);

ALTER TABLE Produit
ADD CONSTRAINT ck_Produit UNIQUE(ref_produit),
ADD CONSTRAINT fk_Produit_Categorie FOREIGN KEY (categorie) REFERENCES Categorie(ref_categorie),
ADD CONSTRAINT fk_Produit_Fournisseur FOREIGN KEY (ref_fournisseur) REFERENCES Fournisseur(ref_fournisseur);

ALTER TABLE Stock
ADD CONSTRAINT fk_Stock_Produit FOREIGN KEY (ref_produit) REFERENCES Produit(ref_produit);

ALTER TABLE Client
ADD CONSTRAINT ck_Client UNIQUE(ref_client);

ALTER TABLE Fournisseur
ADD CONSTRAINT ck_Fournisseur UNIQUE(ref_fournisseur),
ADD CONSTRAINT fk_Fournisseur_Adresse FOREIGN KEY (ref_adresse) REFERENCES Adresse(ref_adresse);

ALTER TABLE Adresse
ADD CONSTRAINT ck_Adresse UNIQUE(ref_adresse);

ALTER TABLE Cadeau
ADD CONSTRAINT ck_Cadeau UNIQUE(ref_cadeau),
ADD CONSTRAINT fk_Cadeau_Produit FOREIGN KEY (ref_produit) REFERENCES Produit(ref_produit);

ALTER TABLE Commande
ADD CONSTRAINT ck_Commande UNIQUE(ref_commande);

ALTER TABLE Ligne_commande
ADD CONSTRAINT ck_Ligne_Commande UNIQUE(ref_ligne),
ADD CONSTRAINT fk_Ligne_Commande_Commande FOREIGN KEY (ref_commande) REFERENCES Commande(ref_commande),
ADD CONSTRAINT fk_Ligne_Commande_Produit FOREIGN KEY (ref_produit) REFERENCES Produit(ref_produit);

ALTER TABLE Panier_recurrent
ADD CONSTRAINT fk_Panier_Recurrent_Commande FOREIGN KEY (ref_commande) REFERENCES Commande(ref_commande),
ADD CONSTRAINT fk_Panier_Recurrent_Client FOREIGN KEY (ref_client) REFERENCES Client(ref_client);

ALTER TABLE C_client
ADD CONSTRAINT ck_C_Client UNIQUE(ref_commande),
ADD CONSTRAINT fk_C_Client_Client FOREIGN KEY (ref_client) REFERENCES Client(ref_client),
ADD CONSTRAINT fk_C_Client_Cadeau FOREIGN KEY (ref_cadeau) REFERENCES Cadeau(ref_cadeau);

ALTER TABLE C_fournisseur
ADD CONSTRAINT ck_C_Fournisseur UNIQUE(ref_commande),
ADD CONSTRAINT fk_C_Fournisseur_Fournisseur FOREIGN KEY (ref_fournisseur) REFERENCES Fournisseur(ref_fournisseur);