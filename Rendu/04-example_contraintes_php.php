<?php
// ....
function check_qte_ligne_commande($ref_produit,$qte)
{
	$response = array();
	$dbcon = pg_connect("dbname=ibd");
	$result = pg_query_params($dbconn, 'SELECT qte_actuelle FROM Stock WHERE ref_produit = $1', array($ref_produit));
	$stock = pg_fetch_array($result, NULL, PGSQL_ASSOC);

	if($stock["qte_actuelle"] < $qte)
		$response['error'] = "La quantité demandée n'est pas disponible en stock.";

	return $response;
}

function check_commande($ref_commande)
{
	$response = array();
	$dbcon = pg_connect("dbname=ibd");
	$result = pg_query_params($dbconn, 'SELECT etat FROM Commande WHERE ref_commande = $1', array($ref_commande));
	$commande = pg_fetch_array($result, NULL, PGSQL_ASSOC);

	if($commande["etat"] == "ECP" OR $commande["etat"] == "ECL" OR $commande["L"])
		$response['error'] = "Vous ne pouvez pas modifier/supprimer cette commande.";

	return $response;
}

// ...
?>