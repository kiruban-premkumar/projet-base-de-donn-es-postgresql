-- Catégories de produits --
Categorie(#ref_categorie, libelle, description, CA)

Sous_categorie(#ref_sous_categorie, libelle, description, CA, categorie=>Categorie) 
-- avec categorie NOT NULL

-- Catégories spéciales et événements --
Categorie_speciale(#ref_categorie_speciale, libelle, description, CA, type)

Categorie_speciale_produit(#ref_categorie=>Categorie_speciale, #ref_produit=>Produit)
-- avec ref_categorie NOT NULL
-- avec ref_categorie IN Categorie_speciale
-- avec ref_produit NOT NULL
-- avec ref_produit IN Produit

Evenement(#ref_evenement, libelle, description, CA)

Promo(#ref_evenement=>Evenement, #ref_produit=>Produit, taux, d_debut, d_fin)
-- avec ref_evenement NOT NULL
-- avec ref_evenement IN Evenement
-- avec ref_produit NOT NULL
-- avec ref_produit IN Produit

-- Produits --
Produit(#ref_produit, libelle, description, d_premier_achat, CA, categorie=>Categorie, ref_fournisseur=>Fournisseur)
-- avec categorie NOT NULL
-- avec ref_fournisseur NOT NULL

-- Stocks --
Stock(#ref_produit=>Produit, qte_min, qte_max, qte_limite, qte_actuelle)
-- avec ref_produit NOT NULL
-- avec toutes les clés de Produit présentes dans Stock

-- Clients --
Client(#ref_client, nom, prenom, d_naissance)

-- Fournisseurs --
Fournisseur(#ref_fournisseur, description, ref_adresse=>Adresse)
-- avec ref_adresse NOT NULL

-- Adresses --
Adresse(#ref_adresse, batiment, numero, voie, adresse_1, adresse_2, code_postal, ville, pays)

-- Cadeaux --
Cadeau(#ref_cadeau, prix_min, ref_produit=>Produit)
-- avec ref_produit NOT NULL

-- Commandes --
Commande(#ref_commande, etat, montant)

Ligne_commande(#ref_commande=>Commande, #ref_ligne, quantite, prix, ref_produit=>Produit)
-- avec ref_produit NOT NULL
-- avec ref_produit IN Produit
-- avec toutes les clés de Commande présentes dans Ligne_commande

Panier_recurrent(#ref_commande=>Commande, ref_client=>Client)
-- avec ref_client NOT NULL

C_client(#ref_commande=>Commande, d_commande, d_livraison, ref_client=>Client, ref_cadeau=>Cadeau)
-- avec ref_client NOT NULL
-- avec toutes les clés de Client présentes dans C_client

C_fournisseur(#ref_commande=>Commande, d_commande, d_livraison, ref_fournisseur=>Fournisseur)
-- avec ref_fournisseur NOT NULL
-- avec toutes les clés de Fournisseur présentes dans C_fournisseur